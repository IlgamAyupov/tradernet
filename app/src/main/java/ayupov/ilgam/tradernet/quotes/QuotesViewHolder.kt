package ayupov.ilgam.tradernet.quotes

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ayupov.ilgam.tradernet.R
import ayupov.ilgam.tradernet.extensions.gone
import ayupov.ilgam.tradernet.extensions.setTextColor
import ayupov.ilgam.tradernet.extensions.visible

class QuotesViewHolder(
    private val view: View
) : RecyclerView.ViewHolder(view) {

    private val tvC = view.findViewById<TextView>(R.id.tvC)
    private val tvLtrAndName = view.findViewById<TextView>(R.id.tvLtrAndName)
    private val tvPcp = view.findViewById<TextView>(R.id.tvPcp)
    private val tvLtpAndChg = view.findViewById<TextView>(R.id.tvLtpAndChg)

    fun bind(quote: Quote) {
        quote.apply {
            tvC.text = c

            if (ltr != null && name != null) {
                tvLtrAndName.text = view.context.getString(R.string.quote_ltr_and_name, ltr, name)
                tvLtrAndName.visible()
            } else if (ltr != null) {
                tvLtrAndName.text = ltr
                tvLtrAndName.visible()
            } else if (name != null) {
                tvLtrAndName.text = name
                tvLtrAndName.visible()
            } else {
                tvLtrAndName.gone()
            }

            when {
                pcp == null -> tvPcp.gone()
                pcp > 0 -> {
                    tvPcp.visible()
                    tvPcp.text = view.context.getString(R.string.quote_pcp, pcp)
                    tvPcp.setTextColor(view.context, R.color.green)
                }
                pcp < 0 -> {
                    tvPcp.visible()
                    tvPcp.text = view.context.getString(R.string.quote_pcp, pcp)
                    tvPcp.setTextColor(view.context, R.color.red)
                }
                else -> {
                    tvPcp.visible()
                    tvPcp.text = view.context.getString(R.string.quote_pcp, pcp)
                    tvPcp.setTextColor(view.context, R.color.black)
                }
            }

            if (ltp != null && chg != null) {
                tvLtpAndChg.text = view.context.getString(R.string.quote_ltp_and_chg, ltp, chg)
                tvLtpAndChg.visible()
            } else if (ltp != null) {
                tvLtpAndChg.text = ltp
                tvLtpAndChg.visible()
            } else if (chg != null) {
                tvLtpAndChg.text = chg
                tvLtpAndChg.visible()
            } else {
                tvLtpAndChg.gone()
            }
        }
    }
}
