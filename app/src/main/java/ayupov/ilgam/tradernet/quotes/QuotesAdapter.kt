package ayupov.ilgam.tradernet.quotes

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ayupov.ilgam.tradernet.R

class QuotesAdapter : RecyclerView.Adapter<QuotesViewHolder>() {

    private val quotes: MutableList<Quote> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotesViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.view_quote, parent, false)
        return QuotesViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: QuotesViewHolder, position: Int) =
        holder.bind(quotes[position])

    override fun getItemCount() = quotes.size

    @SuppressLint("NotifyDataSetChanged")
    fun setQuotes(items: List<Quote>) {
        this.quotes.clear()
        this.quotes.addAll(items)
        notifyDataSetChanged()
    }
}
