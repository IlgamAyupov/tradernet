package ayupov.ilgam.tradernet.quotes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ayupov.ilgam.tradernet.WebSocket
import io.socket.client.Socket
import io.socket.client.Socket.*
import org.json.JSONObject

class QuotesViewModel : ViewModel() {

    companion object {
        private const val EVENT_SUBSCRIBE_QUOTES = "q"
        private const val EVENT_EMIT_QUOTES = "sup_updateSecurities2"
    }

    private val socket: Socket = WebSocket.socket

    private val socketStateLiveData = MutableLiveData(EVENT_CONNECTING)

    private val quotesLiveData = MutableLiveData<List<Quote>?>()

    fun getSocketStateLiveData(): LiveData<String> = socketStateLiveData

    fun getQuotesLiveData(): LiveData<List<Quote>?> = quotesLiveData

    init {
        socket.on(EVENT_CONNECTING) {
            socketStateLiveData.postValue(EVENT_CONNECTING)
        }

        socket.on(EVENT_CONNECT) {
            socketStateLiveData.postValue(EVENT_CONNECT)
            socket.emit(EVENT_EMIT_QUOTES, DesiredQuotesTickers.asJsonArray())
        }

        socket.on(EVENT_CONNECT_ERROR) {
            socketStateLiveData.postValue(EVENT_CONNECT_ERROR)
        }

        socket.on(EVENT_DISCONNECT) {
            socketStateLiveData.postValue(EVENT_DISCONNECT)
        }

        socket.on(EVENT_SUBSCRIBE_QUOTES) { array ->
            for (item in array) {
                if (item is JSONObject) {
                    val jsonArray = item.getJSONArray("q")
                    val quotes = mutableListOf<Quote>()
                    for (i in 0 until jsonArray.length()) {
                        val qObject = jsonArray[i] as JSONObject
                        val quote = Quote(
                            qObject.optString("c"),
                            qObject.optDouble("pcp").toFloat(),
                            qObject.optString("ltr"),
                            qObject.optString("name"),
                            qObject.optString("ltp"),
                            qObject.optString("chg")
                        )
                        quotes.add(quote)
                    }
                    quotesLiveData.postValue(quotes)
                }
            }
        }

        socket.connect()
    }

    override fun onCleared() {
        super.onCleared()
        socket.off()
        socket.disconnect()
    }
}
