package ayupov.ilgam.tradernet.quotes

data class Quote(
    val c: String? = null,      //Тикер
    val pcp: Float? = null,     //Изменение в процентах относительно цены закрытия предыдущей торговой сессии
    val ltr: String? = null,    //Биржа последней сделки
    val name: String? = null,   //Название бумаги
    val ltp: String? = null,    //Цена последней сделки
    val chg: String? = null     //Изменение цены последней сделки в пунктах относительно цены закрытия предыдущей торговой сессии
)
