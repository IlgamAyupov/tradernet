package ayupov.ilgam.tradernet.quotes

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ayupov.ilgam.tradernet.R
import ayupov.ilgam.tradernet.extensions.gone
import ayupov.ilgam.tradernet.extensions.setBackgroundColor
import ayupov.ilgam.tradernet.extensions.visible
import io.socket.client.Socket.*

class QuotesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quotes)

        val tvSocketState = findViewById<TextView>(R.id.tvSocketState)

        val rvQuotes = findViewById<RecyclerView>(R.id.rvQuotes)
        val quotesAdapter = QuotesAdapter()
        val linearLayoutManager = LinearLayoutManager(this)
        rvQuotes.adapter = quotesAdapter
        rvQuotes.layoutManager = linearLayoutManager
        rvQuotes.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        val model: QuotesViewModel = ViewModelProvider(this).get(QuotesViewModel::class.java)

        model.getSocketStateLiveData().observe(this) {
            when (it) {
                EVENT_CONNECTING -> {
                    tvSocketState.visible()
                    tvSocketState.setBackgroundColor(this, R.color.green)
                    tvSocketState.text = getString(R.string.socket_state_connecting)
                }
                EVENT_CONNECT -> {
                    tvSocketState.gone()
                    tvSocketState.setBackgroundColor(this, R.color.green)
                    tvSocketState.text = getString(R.string.socket_state_connect)
                }
                EVENT_CONNECT_ERROR -> {
                    tvSocketState.visible()
                    tvSocketState.setBackgroundColor(this, R.color.red)
                    tvSocketState.text = getString(R.string.socket_state_connect_error)
                }
                EVENT_DISCONNECT -> {
                    tvSocketState.visible()
                    tvSocketState.setBackgroundColor(this, R.color.red)
                    tvSocketState.text = getString(R.string.socket_state_disconnect)
                }
            }
        }

        model.getQuotesLiveData().observe(this) {
            it?.let {
                quotesAdapter.setQuotes(it)
            }
        }
    }
}
