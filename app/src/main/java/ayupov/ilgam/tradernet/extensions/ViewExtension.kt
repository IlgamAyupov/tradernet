package ayupov.ilgam.tradernet.extensions

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.setBackgroundColor(context: Context, colorRes: Int) {
    this.setBackgroundColor(ContextCompat.getColor(context, colorRes))
}
