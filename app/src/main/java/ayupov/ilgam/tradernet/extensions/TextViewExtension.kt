package ayupov.ilgam.tradernet.extensions

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat

fun TextView.setTextColor(context: Context, colorRes: Int) {
    this.setTextColor(ContextCompat.getColor(context, colorRes))
}
